module.exports= function (cd, n){
    return({ret: function() {
        if(!(typeof(cd)=='function')||!Number.isInteger(n)||n==0){
            console.log("give proper arguments");
            return(null);
        }
        else{
        for(let i=0;i<n;i++){
            cd(i+1);
        }}
    }})
}