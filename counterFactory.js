module.exports= function (){
    return({
        increment: function(i){
            if(!(Number.isInteger(i))){
                console.log("Give Numbers");
                return null;
            }
            return i++;
        },
        
        decrement: function(i){
            if(!(Number.isInteger(i))){
                console.log("Give Numbers");
                return null;
            }
            return --i;
        }
    })
}
