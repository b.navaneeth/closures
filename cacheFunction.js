let cache={};
module.exports= function (cd) {
    return(
        { re: function (...s){
            if(!(typeof(cd)=='function')){
                console.log("give proper arguments");
                return (null);
            }
            let f=0;
            for(let i=0;i<s.length;i++){
                s[i]=s[i].toString();
                if(!cache[s[i]]){
                    cache[s[i]]=" ";
                    f=1;
                }
            }
            if(f==0){
                return cache;
            }
            else{
                return cd;
            }
        }
        }
    )
}